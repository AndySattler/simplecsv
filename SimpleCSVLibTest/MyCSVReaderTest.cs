using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleCSVLib;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleCSVLibTest
{
    [TestClass]
    public class MyCSVReaderTest
    {
        [TestMethod]
        [DeploymentItem(@"TestFiles\test.csv", "TestFiles")]
        public async Task Should_Process_The_Test_CSV_File_Correctly()
        {
            MyCSVReader myCSV = new MyCSVReader();
            var result = await myCSV.ReadFileAsync(@"TestFiles\test.csv", 3, SEPARATOR_TYPE.COMMA);

            Assert.AreEqual(1, result.validLines.Count(), "There should be one valid line");
            Assert.AreEqual(2, result.invalidLines.Count(), "There should be two invalid lines");
        }

        [TestMethod]
        [DeploymentItem(@"TestFiles\test.csv", "TestFiles")]
        public async Task Should_Process_The_Test_TAB_File_Correctly()
        {

            MyCSVReader myCSV = new MyCSVReader();
            var result = await myCSV.ReadFileAsync(@"TestFiles\tabtest.csv", 3, SEPARATOR_TYPE.TAB);

            Assert.AreEqual(1, result.validLines.Count(), "There should be one valid line");
            Assert.AreEqual(2, result.invalidLines.Count(), "There should be two invalid lines");
        }
    }
}

﻿using System;
using System.Linq;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using SimpleCSVLib;

namespace SimpleCSVTest
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            //setup dependency injection  overkill for something this small, but I got the impression that this was what was wanted
            var serviceProvider = new ServiceCollection()
           //.AddSingleton<IProcessCSV, CSVReader>() //this one uses a csvReader library
           .AddSingleton<IProcessCSV, MyCSVReader>()
           .AddSingleton<ILogger, Logger>()
           .BuildServiceProvider();


            IProcessCSV csvReader = serviceProvider.GetService<IProcessCSV>();
            ILogger logger = serviceProvider.GetService<ILogger>();

            var inputs = GetInputs(logger);

            if (!inputs.validInputs)
                return;

            try
            {
                var result = await csvReader.ReadFileAsync(inputs.fileLocation, inputs.fieldCount, inputs.delimeterType);
                try
                {
                    string directory = Path.GetDirectoryName(inputs.fileLocation);
                    string extension = Path.GetExtension(inputs.fileLocation);

                    File.WriteAllLines($@"{directory}\validLines{extension}", result.validLines);
                    File.WriteAllLines($@"{directory}\invalidLines{extension}", result.invalidLines);
                }
                catch(Exception ex)
                {
                    logger.WriteError($"Error writing files {ex.Message}");
                    return;
                }
            }
            catch(Exception ex)
            {
                logger.WriteError($"Error processing reading file {ex.Message}");
                return;
            }
        }

        public static (string fileLocation, int fieldCount, SEPARATOR_TYPE delimeterType, bool validInputs) GetInputs(ILogger logger)
        {
            Console.WriteLine("Where is the file Located?");
            string fileLocation = Console.ReadLine();

            if (!File.Exists(fileLocation))
            {
                logger.WriteError("Invalid file location");
                return ("", 0, SEPARATOR_TYPE.COMMA, false);
            }

            Console.WriteLine("Is the file format CSV (Comma-separated values) or TSV (Tab-separated values)?");
            string delimeterType = Console.ReadLine();
            if (!new string[] { "CSV", "TSV" }.Contains(delimeterType.ToUpper()))
            {
                logger.WriteError("Invalid selection.  The values need to be either \"CSV\" or \"TSV\"");
                return ("", 0, SEPARATOR_TYPE.COMMA, false);
            }

            Console.WriteLine("How many fields should each record contain?");
            string numberOfFields = Console.ReadLine();

            if (!int.TryParse(numberOfFields, out int fieldCount))
            {
                logger.WriteError("Number of fields is an invalid input");
                return ("", 0, SEPARATOR_TYPE.COMMA, false);
            }

            return (
                fileLocation, 
                fieldCount, 
                delimeterType.ToUpper() == "CSV" ? SEPARATOR_TYPE.COMMA : SEPARATOR_TYPE.TAB, 
                true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleCSVTest
{
    public interface ILogger
    {
        void WriteError(string message);
    }
    public class Logger : ILogger
    {
        public void WriteError(string message)
        {
            //this would normally get logged to something else (file, db, splunk, etc)
            Console.WriteLine(message);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

namespace SimpleCSVLib
{
    //This class will not handle strings that contain the delimeter or other odd cases that can happen. If you are concerned about those use the CSVReader class above
    public class MyCSVReader : IProcessCSV
    {
        public async Task<(IEnumerable<string> validLines, IEnumerable<string> invalidLines)> ReadFileAsync(string fileLocation, int columnCount, SEPARATOR_TYPE separator)
        {
            List<string> validLines = new List<string>();
            List<string> invalidLines = new List<string>();
            string delimeter = separator == SEPARATOR_TYPE.COMMA ? "," : "\t";
            bool isHeader = true;

            foreach (string line in await File.ReadAllLinesAsync(fileLocation))
            {
                if (isHeader)
                {
                    isHeader = false;
                }
                else
                {
                    if (line.Split(delimeter).Length == columnCount)
                        validLines.Add(line);
                    else
                        invalidLines.Add(line);
                }
            }

            return (validLines, invalidLines);
        }
    }
}

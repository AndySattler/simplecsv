﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SimpleCSVLib
{
    public class CSVReader : IProcessCSV
    {
        //I would use an already existing CSV Reader like this because of the complexity of data in csv files, but I think this might be cheating from what you want so I am writing my own simple one below
        public async Task<(IEnumerable<string> validLines, IEnumerable<string> invalidLines)> ReadFileAsync(string fileLocation, int columnCount, SEPARATOR_TYPE separator)
        {
            List<string> validLines = new List<string>();
            List<string> invalidLines = new List<string>();            
            var csv = await File.ReadAllTextAsync(fileLocation);

            //Separator type is auto detected so I don't pass that in
            foreach (var line in Csv.CsvReader.ReadFromText(csv))
            {
                // Header is handled, each line will contain the actual row data
                if (line.ColumnCount == columnCount)
                    validLines.Add(line.Raw);
                else
                    invalidLines.Add(line.Raw);
            }

            return (validLines, invalidLines);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleCSVLib
{
    public enum SEPARATOR_TYPE { COMMA, TAB }

    public interface IProcessCSV
    {
        Task<(IEnumerable<string> validLines, IEnumerable<string> invalidLines)> ReadFileAsync(string fileLocation, int columnCount, SEPARATOR_TYPE separator);
    }
}
